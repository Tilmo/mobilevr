using UnityEngine;
using OculusSampleFramework;
using UnityEngine.Events;

[RequireComponent(typeof(MeshRenderer))]
public class RayInteractionExample : MonoBehaviour
{
	[SerializeField] private Material baseMaterial;
	[SerializeField] private Material actionMaterial;
	[SerializeField] private Material hoverMaterial;

	[SerializeField] private ButtonController button;
	[SerializeField] private UnityEvent actionEvent;

	private MeshRenderer meshRenderer;
	private InteractableTool interactableTool;

	private void Start()
	{
		meshRenderer = GetComponent<MeshRenderer>();
		button.InteractableStateChanged.AddListener(StateChanged);
	}
	private void StateChanged(InteractableStateArgs obj)
	{
		if (obj.NewInteractableState == InteractableState.ActionState)
			actionEvent.Invoke();

		if (obj.NewInteractableState > InteractableState.Default)
			interactableTool = obj.Tool;
		else
			interactableTool = null;
	}
	private void Update()
	{
		if (interactableTool == null)
		{
			meshRenderer.material = baseMaterial;
		}
		else
		{
			if (interactableTool.ToolInputState == ToolInputState.PrimaryInputDown ||
				interactableTool.ToolInputState == ToolInputState.PrimaryInputDownStay)
				meshRenderer.material = actionMaterial;
			else
				meshRenderer.material = hoverMaterial;
		}
	}
}