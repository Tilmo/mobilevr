﻿using UnityEngine;

[RequireComponent(typeof(OVRHand))]
//Inherit OVRGrabber to access grab functions
public class HandGrabbingBehaviour : OVRGrabber
{
	[SerializeField] private float pinchThreshold = 0.7f;
	private OVRHand hand;

	protected override void Start()
	{
		base.Start();
		hand = GetComponent<OVRHand>();
	}

	// Update is called once per frame
	public override void Update()
	{
		base.Update();
		CheckIndexPinch();
	}

	void CheckIndexPinch()
	{
		//Pinch strength of index finger pinch
		float pinchStrength = hand.GetFingerPinchStrength(OVRHand.HandFinger.Index);

		//Check if pinch strength exceeds threshold 
		if (!m_grabbedObj && pinchStrength > pinchThreshold && m_grabCandidates.Count > 0)
			GrabBegin();
		else if (m_grabbedObj && !(pinchStrength > pinchThreshold))
			GrabEnd();
	}
}
