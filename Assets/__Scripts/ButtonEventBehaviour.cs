using OculusSampleFramework;
using UnityEngine;
using UnityEngine.Events;

public class ButtonEventBehaviour : MonoBehaviour
{
	[SerializeField] private UnityEvent buttonActionEvent;
	public void InvokeAction(InteractableStateArgs obj)
	{
		if (obj.NewInteractableState == InteractableState.ActionState)
			buttonActionEvent.Invoke();
	}
}
